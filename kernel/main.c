/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author: Olivier Gruber (olivier dot gruber at acm dot org)
*/

#include "main.h"

int i=0;

//to maintain cursor position in terminal mode
int cursorX=0;
int cursorY=0;
//to maintain cursor position in screen mode
int cursorPos=0;

void kmain(void) {
	
  /*
  ######################################################################################
  #  Note : Uncomment (ctrl+sift+/ in azerty keyboard) the section you want to execute #
  #                       Only one section should be executed !!!!                     #
  #                                                                                    #
  ######################################################################################
  */

  ////6.1 section : Discovering the environnement
  // serial_init(COM1);
	// serial_send_string(COM1,"\n\rHello!\n\r\n");
  // serial_send_string(COM1,"This is a simple echo console... please type something.\n\r\n\r");
  // while(1) {
  //  unsigned char c;
	// 	c=serial_receive(COM1);
	// 	if (c==13) {
	// 		c = '\r';
	// 		serial_send(COM1,c);
	// 		c = '\n';
	// 		serial_send(COM1,c);
	// 	} else 
	// 		serial_send(COM1,c);
  // }

  ////6.2 section : implementing kprintf
  // serial_init(COM1);
  // while(1) {
  //   unsigned char c;
  //   c=serial_receive(COM1);
  //   kprintf("[%c] %d \n",c,(int)c);
  // }

  ////6.2 section : cursor move, CTRL+X to clear, backspace support
  // serial_init(COM1);
  // clearConsole();
  // while(1) {
  //  unsigned char c;
	// 	c=serial_receive(COM1);
	// 	if (c==13) { // enter
  //     cursorX++;
  //     cursorY = 0;
	// 	} else if(c == 24){ // ctrl + x
  //     clearConsole();
  //   } else if(c == 127){ // backspace
  //     //backward the cursor of 1 position
  //     moveCursor(cursorX, cursorY--);
  //     //send ' ' character
  //     serial_send(COM1,' ');
  //   } else if(c == 27){ //command detection ESC
  //       c = serial_receive(COM1);
  //     if(c == '['){ // [
  //       c = serial_receive(COM1);
  //         if(c == 'B'){ // up
  //           cursorX++;
  //         }else if(c == 'A'){ // down
  //           cursorX--;
  //         }else if(c == 'C'){ // right
  //           cursorY++;
  //           //kprintf("[%d]\n",cursorY);
  //         }else if(c == 'D'){ // left
  //           cursorY--;
  //         }
  //         //cursor limit control
  //         if(cursorX > 24) cursorX = 24;
  //         if(cursorY > 80) cursorY = 80;
  //         if(cursorX < 0) cursorX = 0;
  //         if(cursorY < 0) cursorY = 0;
  //         //cursor apply setting
  //         moveCursor(cursorX, cursorY);
  //     }
  //   }else{
  //     if(cursorY > 80){
  //       if(cursorX < 24){
  //         cursorX++;
  //         cursorY = 0;
  //         //kprintf("[%c]\n",cursorX+'0');
  //       }
  //     }
  //     moveCursor(cursorX, cursorY);
  //     serial_send(COM1,c);
  //     cursorY++;
  //   }

  ////6.3 section : echoing characters to screen and CTRL+X to clear screen
  // serial_init(COM1);
  // clearScreen();
  // while(1) {
  //   unsigned char c;
  // 	c=serial_receive(COM1);
  //   if (c==13) {
	// 		c = '\r';
	// 		serial_send(COM1,c);
	// 		c = '\n';
	// 		serial_send(COM1,c);
  //   } else if(c == 24){ // ctrl + x
  //     clearScreen();
  //     cursorPos=0;
  //   } else if(c == 127){ // backspace

	// 	} else{
	// 		serial_send(COM1,c);
  //     //video_write(0x2a,c);
  //     cursorPos++;
  //     video_write(0x2a,c);
  //   }
  // }

  ////6.3 section : implementing printf
  // serial_init(COM1);
  // while(1) {
  //   unsigned char c;
  //   c=serial_receive(COM1);
  //   printf("[%c] %d\n",c,(int) c);
  // }

}




//Utility functions

//Move the cursor within the terminal easily
void moveCursor(int x, int y){
  char seqX[2]={x+'0','-'};
  char seqY[2]={y+'0','-'};

  //Split the number if it is > 9 and then we convert it to char
  if(x > 9){
    seqX[0] = (x / 10) + '0';
    seqX[1] = (x % 10) + '0';
  }

  if(y > 9){
    seqY[0] = (y / 10) + '0';
    seqY[1] = (y % 10) + '0';
  }

  //Send the sequence of characters
  serial_send(COM1,27);
  serial_send(COM1,'[');
  for(int i=0;i<2;i++) seqX[i] != '-' ? serial_send(COM1,seqX[i]) : ' ';
  serial_send(COM1,';');
  for(int i=0;i<2;i++) seqY[i] != '-' ? serial_send(COM1,seqY[i]) : ' ';
  serial_send(COM1,'f');
}

//Clear the terminal
void clearConsole(){
  moveCursor(cursorX = 0, cursorY = 0);
  serial_send(COM1,27);
  serial_send(COM1,'[');
  serial_send(COM1,'2');
  serial_send(COM1,'J');
}

//Clear the screen emulated by Qemu
void clearScreen(){
  for(int i=0;i<(25*80)-cursorPos; i++){
    video_write(0x00,'F');  
  }
}

//Used in kprintf
void kputchar(char c){
  serial_send(COM1,c);
}

//Used in printf
void sputchar(char c){
  video_write(0x2a,c);
}